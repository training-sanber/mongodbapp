# mongodbapp

ada 3 Microservice 1 dengan FastApi+mongodb dan 2 menggunakan Flask+mysql yang bertujuan untuk membuat Aplikasi Perpustakaan

ini adalah Microservice menggunakan FastApi+mongodb

Ini Microservice Untuk menampilkan Daftar buku yang tersedia.

Microservice ini menggunakan Python 3.9.6, OpenApi 
Disini menggunakan database mongodb

## Instalasi 

Untuk Instalasi silahkan buat Virtualenv, unduh microservice lalu install requirement

```
git clone https://gitlab.com/training-sanber/mongodbapp.git

cd mongodbapp

pip install -r requirement.txt
```

folder dump database yaitu "perpustakaandatabase" silahkan restore.

Cara menjalankan

```
uvicorn main:app --reload
```

RestApi yang tersedia yaitu 

## 1. Menampilkan buku berdasarkan ID

```
localhost:8000/bookbyid/
```

Cara menggunakan dengan method "POST" serta dengan format JSON :
```
{
    "id":"idbuku"
}
```

## 2. Menampilkan buku berdasarkan Nama
```
localhost:8000/bookbyname/
```

Cara menggunakan dengan method "POST" serta dengan format JSON :
```
{
    "bookname":"namabuku"
}
```

## 3. Menampilkan semua buku
```
localhost:8000/books/
```
Cara menggunakan dengan method "POST".